# README #

AUTHOR: Logan Levitre, llevitre@uoregon.edu

Installation:

Nothing needed to be installed

How-To:

Pull entire folder to you computer/laptop

place credentials.ini into folder

enter make start

open web browser and enter localhost:[Port]

[Port] will be displayed in shell, enter that port to display page

goto tests folder

read README.md within tests

run tests.sh with localhost:port

to stop use stop.sh


Changes:
thing altered within folder was pageserver.py. Edited code in the respond function to check 
for ".." "~" and "//" within a URL. if any are found it will throw a 403 error. Also added 
a check if pages does not contain "*.html" or "*.css" it will throw a 404 error. 
And added to it that if the URL ends with name.html or name.css send content of name.html or name.css 
with proper http response.


